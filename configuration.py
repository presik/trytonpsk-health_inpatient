from trytond.model import fields
from trytond.pool import PoolMeta


class HealthConfiguration(metaclass=PoolMeta):
    'Health Configuration'
    __name__ = 'health.configuration'

    inpatient_registration_sequence = fields.Many2One('ir.sequence',
        'Inpatient Sequence')
    inpatient_meal_order_sequence = fields.Many2One('ir.sequence',
        'Inpatient Meal Sequence')
