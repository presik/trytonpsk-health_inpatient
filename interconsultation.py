from datetime import datetime, date
import pytz

from trytond.model import ModelView, ModelSQL, fields, Unique, Workflow
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids
from sql import Literal
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Not, Bool, And, Equal, Or
from trytond.i18n import gettext
from trytond.report import Report
from trytond.exceptions import UserError, UserWarning


class Interconsultation(Workflow, ModelSQL, ModelView):
    'Interconsultation'
    __name__ = 'health.interconsultation'

    STATES_REQUEST = {'readonly': Eval('state') != 'draft'}
    STATES_ANSWER = {'readonly': Eval('state') != 'requested'}

    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': Eval('state') != 'draft'}, depends=['state'])
    number = fields.Char('Number', readonly=True)
    attending_date = fields.DateTime('Attending Date', readonly=True)
    specialty = fields.Many2One('health.specialty', 'Specialty', required=True)
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True, readonly=True)
    requested_doctor = fields.Many2One('health.professional',
        'Requested Doctor')
    request_note = fields.Text('Cause', states=STATES_REQUEST, required=True, select=False)
    answer_note = fields.Text('Answer', states=STATES_ANSWER)
    product = fields.Many2One('product.product', 'Consult Product',
        domain=[('type', '=', 'service')], readonly=True)
    answer_doctor = fields.Many2One('health.professional',
        'Answer Doctor', readonly=True)
    answer_date = fields.DateTime('Answer Date', readonly=True)
    state = fields.Selection([
        (None, ''),
        ('draft', 'Draft'),
        ('requested', 'Requested'),
        ('done', 'Done'),
        ], 'Status', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Interconsultation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('number_uniq', Unique(t, t.number),
             'The test number code must be unique')
        ]
        cls._order.insert(0, ('attending_date', 'DESC'))
        cls._buttons.update({
            'answer': {
                'invisible': Eval('state').in_(['done'])
            },
        })

        cls._transitions |= set((
            ('draft', 'requested'),
            ('requested', 'done'),
        ))
        cls._order.insert(0, ('attending_date', 'DESC'))
        cls._order.insert(1, ('number', 'DESC'))

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        config = pool.get('health.configuration').get_config()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('number'):
                values['number'] = config.interconsultation_request_sequence.get()
                values['product'] = config.product_interconsultation
            values['state'] = 'requested'

        return super(Interconsultation, cls).create(vlist)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('patient',) + tuple(clause[1:]),
            ('number',) + tuple(clause[1:]),
        ]

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.inpatient']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def answer(cls, records):
        for rec in records:
            rec.set_answer_doctor()
            rec.set_answer_date()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_attending_date():
        return datetime.now()

    @staticmethod
    def default_attending_physician():
        Professional = Pool().get('health.professional')
        attending_physician = str(Professional.get_health_professional())
        return attending_physician

    def set_answer_doctor(self):
        Professional = Pool().get('health.professional')
        self.answer_doctor = Professional.get_health_professional()
        self.save()

    def set_answer_date(self):
        self.answer_date = datetime.now()
        self.save()
