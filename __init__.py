# -*- coding: utf-8 -*-

from trytond.pool import Pool
from . import inpatient
from . import configuration
from . import shift
from . import interconsultation
from .wizard import wizard_inpatient_evaluation, wizard_health_inpatient


def register():
    Pool.register(
        configuration.HealthConfiguration,
        inpatient.DietTherapeutic,
        inpatient.Inpatient,
        inpatient.BedTransfer,
        inpatient.HealthService,
        inpatient.PatientEvaluation,
        inpatient.ECG,
        inpatient.InternalProcedure,
        inpatient.MedicalSupply,
        inpatient.PatientData,
        inpatient.InpatientMedication,
        inpatient.InpatientMedicationLog,
        inpatient.InpatientDiet,
        inpatient.InpatientMeal,
        inpatient.InpatientMealOrder,
        inpatient.InpatientMealOrderItem,
        inpatient.HealthLabTest,
        inpatient.ImagingTestRequest,
        inpatient.Evolutions,
        inpatient.NewBorn,
        inpatient.MedicTeam,
        shift.InpatientShift,
        interconsultation.Interconsultation,
        wizard_health_inpatient.CreateBedTransferInit,
        module='health_inpatient', type_='model')
    Pool.register(
        wizard_health_inpatient.CreateBedTransfer,
        wizard_inpatient_evaluation.CreateInpatientEvaluation,
        module='health_inpatient', type_='wizard')
    Pool.register(
        inpatient.EvolutionsReport,
        module='health_inpatient', type_='report')
