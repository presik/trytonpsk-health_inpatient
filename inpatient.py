from datetime import datetime, date
import pytz

from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids
from sql import Literal
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Not, Bool, And, Equal, Or
from trytond.i18n import gettext
from trytond.report import Report
from trytond.exceptions import UserError, UserWarning

sequences = [
    'inpatient_registration_sequence', 'inpatient_meal_order_sequence'
]

YES_NO = [
    (None, ''),
    ('yes', 'Yes'),
    ('no', 'No'),
    ]

DISCHARGE_DESTINATION = [
    ('', ''),
    ('house', 'House'),
    ('hospitalization', 'Hospitalization'),
    ('transfer to general operating room', 'Transfer to general operating room'),
    ('transfer to other center', 'Transfer to other center'),
    ('resuscitation room', 'Resuscitation room'),
    ('rehydration room', 'Rehydration room'),
    ('post-emergency room', 'Post-emergency room'),
    ('curettage room', 'Curettage room'),
    ('nebulization room', 'Nebulization room'),
    ('external referral', 'External referral'),
    ('morgue', 'Morgue'),
    ('leakage', 'Leakage'),
    ('consultation', 'Consultation'),
]

STATE_DISCHARGE = [
    ('', ''),
    ('conscious', 'Conscious'),
    ('unconscious', 'Unconscious'),
    ('coma', 'Coma'),
    ('dead', 'Dead'),
]


class DietTherapeutic (ModelSQL, ModelView):
    'Diet Therapy'
    __name__ = "health.diet.therapeutic"

    name = fields.Char('Diet type', required=True, translate=True)
    code = fields.Char('Code', required=True)
    description = fields.Text('Indications', required=True, translate=True)

    @classmethod
    def __setup__(cls):
        super(DietTherapeutic, cls).__setup__()

        t = cls.__table__()
        cls._sql_constraints = [
            ('code_unique', Unique(t, t.code), 'The Diet code already exists')]


class Inpatient(ModelSQL, ModelView):
    'Hospitalization'
    __name__ = 'health.inpatient'

    STATES = {
        'readonly': Or(Eval('state') == 'done', Eval('state') == 'finished')
    }
    STATES_ADMISSION = {'readonly': Eval('state') != 'free'}
    code = fields.Char('Registration Code', readonly=True, select=True)
    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states=STATES_ADMISSION)
    insurance_plan = fields.Many2One('health.insurance.plan',
        'Insurance Plan', states=STATES_ADMISSION)
    admission_kind = fields.Selection([
        (None, ''),
        ('hospitalization', 'Hospitalization'),
        ('emergency', 'Emergency'),
        ('external_consultation', 'External Consultation'),
        ('procedure_scheduling', 'Procedure Scheduling'),
        ('ambulatory', 'Ambulatory'),
        ], 'Admission Type', required=True, select=True, states=STATES_ADMISSION)
    admission_type = fields.Selection([
        (None, ''),
        #('routine', 'Routine'),
        #('maternity', 'Maternity'),
        #('elective', 'Elective'),
        #('urgent', 'Urgent'),
        ('hospitalization', 'Hospitalization'),
        ('emergency', 'Emergency'),
        ('external_consultation', 'External Consultation'),
        ('procedure_scheduling', 'Procedure Scheduling'),
        ('ambulatory', 'Ambulatory'),
        ], 'Admission Type', readonly=True, select=True, states=STATES_ADMISSION)
    hospitalization_date = fields.DateTime('Hospitalization date',
        required=True, select=True, states=STATES_ADMISSION)
    discharge_date = fields.DateTime('Expected Discharge Date', required=True,
        states=STATES)
    discharge_hospitalization_date = fields.DateTime('Discharged Date', readonly=True,
        states=STATES_ADMISSION)
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', states=STATES)
    operating_physician = fields.Many2One('health.professional',
        'Operating Physician', states=STATES)
    admission_reason = fields.Many2One('health.pathology',
        'Reason for Admission', help="Reason for Admission",  states=STATES_ADMISSION,
        select=True)
    admission_diagnosis_1 = fields.Many2One('health.pathology', 'Admission diagnosis',
        states={
            'readonly': Eval('state').in_(['discharged', 'cancelled']),
            'required': Eval('state') == 'discharged',
        }, depends=['state'])
    admission_diagnosis_2 = fields.Many2One('health.pathology', 'Diagnosis 2',
        states=STATES)
    admission_diagnosis_3 = fields.Many2One('health.pathology', 'Diagnosis 3',
        states=STATES)
    bed = fields.Many2One('health.hospital.bed', 'Hospital Bed',
        states={
            'required': Not(Bool(Eval('code'))),
            'readonly': Or(
                Eval('state') == 'hospitalized',
                Eval('state') == 'done',
                Eval('state') == 'finished',

            )
        },
        depends=['patient'])
    ward = fields.Function(fields.Many2One('health.hospital.ward',
        'Hospital Ward'), getter='get_ward', searcher='searcher_ward')
    nursing_plan = fields.Text('Nursing Plan', states=STATES)
    medications = fields.One2Many('health.inpatient.medication', 'registration',
        'Medications', states=STATES)
    lab_requests = fields.One2Many('health.lab_test', 'origin',
        'Tests Lab. Requests', states=STATES, context={
            'patient': Eval('patient', -1),
        }, domain=[
            ('patient', '=', Eval('patient')),
            ('priority', '=', 'urgent'),
        ])
    imaging_requests = fields.One2Many('health.imaging.request', 'origin',
        'Images Request', states=STATES,
            domain=[
                ('patient', '=', Eval('patient')),
                ('urgent', '=', True),
            ], context={'patient': Eval('patient', -1)}
        )
    therapeutic_diets = fields.One2Many('health.inpatient.diet', 'registration',
        'Meals / Diet Program', states=STATES)
    evolutions = fields.One2Many('health.inpatient.evolutions', 'registration',
        'Evolutions', states=STATES)
    nutrition_notes = fields.Text('Nutrition notes / directions', states=STATES)
    discharge_plan = fields.Text('Discharge Plan', states=STATES)
    info = fields.Text('Notes', states=STATES)
    state = fields.Selection((
        (None, ''),
        ('free', 'Free'),
        ('cancelled', 'Cancelled'),
        ('confirmed', 'Confirmed'),
        ('hospitalized', 'Hospitalized'),
        ('done', 'Discharged - needs cleaning'),
        ('finished', 'Finished'),
        ), 'Status', select=True, readonly=True)
    bed_transfers = fields.One2Many('health.bed.transfer', 'registration',
        'Transfer History', readonly=True)
    discharged_by = fields.Many2One('health.professional', 'Discharged by',
        readonly=True,
        help="Health Professional that discharged the patient")
    discharge_reason = fields.Selection([
        (None, ''),
        ('medical_orders', 'Medical Orders'),
        ('home', 'Home / Selfcare'),
        ('referred', 'Referred'),
        ('transfer', 'Transferred to another institution'),
        ('death', 'Death'),
        ('death_24h', 'Death 24hrs'),
        ('death_48h', 'Death 48hrs'),
        ('leakage', 'Leakage'),
        ('volunteer', 'Volunteer')],
        'Discharge Reason',
        states={'readonly': Not(Equal(Eval('state'), 'hospitalized'))},
        help="Reason for patient discharge")
    discharge_dx = fields.Many2One('health.pathology', 'Discharge Dx',
        help="Code for Discharge Diagnosis",
        states={'readonly': Not(Equal(Eval('state'), 'hospitalized'))})
    diagnosis_2 = fields.Many2One('health.pathology', 'Diagnosis 2',
        states=STATES)
    diagnosis_3 = fields.Many2One('health.pathology', 'Diagnosis 3',
        states=STATES)
    discharge_condition = fields.Text('Discharge Condition', states=STATES)
    autopsy = fields.Boolean('Autopsy', states=STATES)
    autopsy_description = fields.Char('Autopsy Diagnosis', states={
        'required': Bool(Eval('autopsy')),
        'invisible': ~Bool(Eval('autopsy'))
    })
    discharge_destination = fields.Selection(DISCHARGE_DESTINATION,
        'Discharge Destination', depends=['state'], states={
            'readonly': Eval('state').in_(['done', 'cancelled']),
            'required': Eval('state') == 'done',
        })
    discharge_destination_string = discharge_destination.translated(
        'discharge_destination')
    state_discharge = fields.Selection(STATE_DISCHARGE, 'State Discharge', states=STATES)
    state_discharge_string = state_discharge.translated('state_discharge')
    discharge_attending_physician = fields.Many2One(
        'health.professional', 'Discharge Attending Physician',
        states=STATES)
    medic_team = fields.One2Many('health.inpatient.medic_team',
        'registration', 'Medic Team', states=STATES)
    institution = fields.Many2One('health.institution', 'Institution',
        readonly=True)
    wound_infection = fields.Boolean('Wound Infection', states=STATES)
    preoperative_mortality = fields.Boolean('Preoperative Mortality (10 D)', states=STATES)
    intraoperative_mortality = fields.Boolean('Intraoperative Mortality (6 hrs)', states=STATES)
    puid = fields.Function(fields.Char('PUID', help="Person Unique Identifier"),
        'get_patient_puid', searcher="search_patient_puid")
    medical_supply = fields.One2Many('health.medical_supply', 'origin',
        'Medical Supply', states=STATES)
    procedures = fields.One2Many('health.internal_procedure', 'origin',
        'Procedures', states=STATES)
    evaluations = fields.One2Many('health.patient.evaluation', 'origin',
        'Evaluations', states=STATES,
            domain=[
                ('patient', '=', Eval('patient')),
                ('evaluation_type', '=', 'inpatient'),
                ('visit_type', '=', 'followup'),
            ], context={'patient': Eval('patient', -1)}
        )
    health_service = fields.Many2One('health.service', 'Health Service',
        help="Relation to health service created", select=True,
        states={'readonly': True})
    hospitalization_location = fields.Many2One('stock.location',
        'Hospitalization Location', domain=[('type', '=', 'warehouse')],
        states={
            'required': Bool(Eval('medications')),
            'readonly': Eval('state') == 'finished',
        }, depends=['state'])
    shipments = fields.Function(fields.One2Many('stock.shipment.out', None,
        'Shipments'), 'get_shipments')
    abortions = fields.Integer('Abortions', states=STATES)
    caesarean = fields.Integer('Caesarean', states=STATES)
    pregnancies = fields.Integer('Pregnancies', states=STATES)
    t_gestations = fields.Integer('T. Gestations', states=STATES)
    blood_type = fields.Char('Blood Type / RH', states=STATES)
    births = fields.Integer('Births', states=STATES)
    date_last_menstruation = fields.DateTime('DLM', states=STATES)
    date_last_papsmear = fields.DateTime('FPP', states=STATES)
    dilatation = fields.Char('Dilatation', states=STATES)
    presentation = fields.Char('Presentation', states=STATES)
    #Antecedents
    disease_history = fields.Text('Disease History', states=STATES)
    childhood_history = fields.Text('Childhood', states=STATES)
    adolescence_history = fields.Text('Adolescence', states=STATES)
    adulthood_history = fields.Text('Adulthood', states=STATES)
    family_history = fields.Text('Family History', states=STATES)
    common_medication = fields.Text('Common Medication', states=STATES)
    blood_transfusion = fields.Text('Blood Transfusion', states=STATES)
    #
    stillborn = fields.Selection(YES_NO, 'Stillborn', states=STATES)
    congenital_anomalies = fields.Selection(YES_NO, 'Cogenital Anomalies', states=STATES)
    neonatal_deaths = fields.Selection(YES_NO, 'Neonatal Deaths', states=STATES)
    syphilis = fields.Selection(YES_NO, 'Syphilis', states=STATES)
    rubella = fields.Selection(YES_NO, 'Rubella', states=STATES)
    measles = fields.Selection(YES_NO, 'Measles', states=STATES)
    hypertarterial = fields.Selection(YES_NO, 'Hypetarterial', states=STATES)
    goiter = fields.Selection(YES_NO, 'Goiter', states=STATES)
    diabetes = fields.Selection(YES_NO, 'Diabetes', states=STATES)
    falcemia = fields.Selection(YES_NO, 'Falcemia', states=STATES)
    condylomatosis = fields.Selection(YES_NO, 'Condylomatosis', states=STATES)
    epilepsy = fields.Selection(YES_NO, 'Epilepsy', states=STATES)
    heart_disease = fields.Selection(YES_NO, 'Heart Disease', states=STATES)
    varicella = fields.Selection(YES_NO, 'Varicella', states=STATES)
    other_antecedents = fields.Char('Others', states=STATES)

    #Antecedents-Toxic Habits
    coffee = fields.Selection(YES_NO, 'Coffee', states=STATES)
    coffee_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('coffee', '') != 'yes')
    })
    smoke = fields.Selection(YES_NO, 'Smoke', states=STATES)
    smoke_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('smoke', '') != 'yes')
    })
    alcohol = fields.Selection(YES_NO, 'Alcohol', states=STATES)
    alcohol_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('alcohol', '') != 'yes')
    })
    other_habits = fields.Char('Other Habits', help="For example drugs consume", states=STATES)
    medications_allergy = fields.Text('Medications Allegy', states=STATES)
    food_allergy = fields.Text('Food Allegy', states=STATES)
    other_allergy = fields.Text('Other Allegy', states=STATES)
    surgical_history = fields.Text('Surgical', states=STATES)
    trauma_history = fields.Text('Trauma', states=STATES)
    #Phisical Exam
    physical_state = fields.Text('Physical', states=STATES)
    neurological_state = fields.Text('Neurological', states=STATES)
    head_neck_state = fields.Text('Head Neck', states=STATES)
    thorax_state = fields.Text('Thorax', states=STATES)
    heart_state = fields.Text('Heart', states=STATES)
    lungs_state = fields.Text('Lungs', states=STATES)
    abdomen_state = fields.Text('Abdomen', states=STATES)
    pelvis_state = fields.Text('Pelvis', states=STATES)
    vaginal_examination = fields.Text('Vaginal Examination', states=STATES)
    extremities_state = fields.Text('Extremities', states=STATES)
    other_state = fields.Text('Other State', states=STATES)
    disability = fields.Boolean('Disability', states=STATES)
    disability_description = fields.Text('Disability Description', states={
        'required': Bool(Eval('disability')),
        'invisible': ~Bool(Eval('disability'))
    })
    bmi = fields.Float('BMI', digits=(2, 2), help='Body mass index',
        states=STATES)
    weight = fields.Float('Weight/Kg', digits=(3, 2), help='Weight in kilos',
        states=STATES)
    height = fields.Float('Height/Cms', digits=(3, 1), states=STATES,
        help='Height in centimeters')
    edema = fields.Boolean('Edema', states=STATES)
    edema_description = fields.Text('Edema Description', states={
        'required': Bool(Eval('edema')),
        'invisible': ~Bool(Eval('edema'))
    })
    healthy = fields.Boolean('Healthy', states=STATES)
    very_sick = fields.Boolean('Very Sick', states=STATES)
    chronically_sick = fields.Boolean('Chronically Sick', states=STATES)
    #Interconsultations
    interconsultations = fields.One2Many('health.interconsultation',
        'origin', 'Interconsultations', states=STATES)
    #VitalSigns
    blood_pressure = fields.Char('Blood Pressure (mm/Hg)',
        help='Reference Values: \n'
            'Normal Values: Sistólica: 116+/-12 Diastólica: 70+/-7 \n'
            'Minimal Values: Sistólica: 90 Diastólica: 60 \n'
            'Alert Values: Sistólica: 130 Diastólica: 90 \n'
            'Emergency Values: Sistólica: 160 Diastólica: 110', states=STATES)
    heart_rate = fields.Integer(
        'Heart Rate', help='Heart rate expressed in beats per minute', states=STATES)
    temperature = fields.Float('Temperature', help='Temperature in celcius', states=STATES)
    respiratory_rate = fields.Integer('Respiratory Rate',
        help='Respiratory rate expressed in breaths per minute', states=STATES)
    heart_rate_fetal = fields.Integer(
        'FCF', help='Heart rate fetal expressed in beats per minute', states=STATES)
    pulse = fields.Integer('Pulse', states=STATES)
    #Exam by Systems
    respiratory_system  = fields.Text('Respiratory System ', states=STATES)
    cardiovascular_system = fields.Text('Cardiovascular System', states=STATES)
    digestive_system = fields.Text('Digestive System', states=STATES)
    endocrine_system = fields.Text('Endocrine System', states=STATES)
    genitourinary_system = fields.Text('Genitourinary System', states=STATES)
    reproductive_system = fields.Text('Reproductive System', states=STATES)
    other_system = fields.Text('Others', states=STATES)
    #NewBorn Page
    newborn_data = fields.One2Many('health.inpatient.newborn',
        'registration', 'NewBorn Receivement', states=STATES)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    def get_patient_puid(self, name):
        return self.patient.party.puid

    @staticmethod
    def default_hospitalization_date():
        return datetime.now()

    @staticmethod
    def default_alcohol():
        return 'no'

    @staticmethod
    def default_smoke():
        return 'no'

    @staticmethod
    def default_coffee():
        return 'no'
#Pathological Antecedents
    @staticmethod
    def default_stillborn():
        return 'no'

    @staticmethod
    def default_congenital_anomalies():
        return 'no'

    @staticmethod
    def default_neonatal_deaths():
        return 'no'

    @staticmethod
    def default_syphilis():
        return 'no'

    @staticmethod
    def default_rubella():
        return 'no'

    @staticmethod
    def default_measles():
        return 'no'

    @staticmethod
    def default_hypertarterial():
        return 'no'

    @staticmethod
    def default_goiter():
        return 'no'

    @staticmethod
    def default_diabetes():
        return 'no'

    @staticmethod
    def default_falcemia():
        return 'no'

    @staticmethod
    def default_condylomatosis():
        return 'no'

    @staticmethod
    def default_epilepsy():
        return 'no'

    @staticmethod
    def default_heart_disease():
        return 'no'

    @staticmethod
    def default_varicella():
        return 'no'

    @classmethod
    def search_patient_puid(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.party.puid', clause[1], value))
        return res

    @fields.depends('weight', 'height')
    def on_change_with_bmi(self):
        if self.height and self.weight:
            if (self.height > 0):
                return round(self.weight / ((self.height / 100) ** 2),2)
            return 0

    @fields.depends('weight', 'height', 'bmi')
    def on_change_bmi(self):
        if self.height and self.weight:
            if (self.height > 0):
                self.bmi = round(self.weight / ((self.height / 100) ** 2), 2)
        elif (self.height and not self.weight):
            self.weight = round((((self.height / 100) ** 2) * self.bmi), 2)

        elif (self.weight and not self.height):
            self.height = round(((self.weight / self.bmi)**(0.5)*100), 2)

    @classmethod
    def __setup__(cls):
        super(Inpatient, cls).__setup__()
        cls._buttons.update({
            'confirmed': {
                'invisible':
                    And(Not(Equal(Eval('state'), 'free')),
                    Not(Equal(Eval('state'), 'cancelled'))),
            },
            'cancel': {
                'invisible': Eval('state').in_(
                    ['cancelled', 'free', 'done', 'finished']),
                    },
            'admission': {
                'invisible': Not(Equal(Eval('state'), 'confirmed')),
            },
            'discharge': {
                'invisible': Not(Equal(Eval('state'), 'hospitalized')),
            },
            'bedclean': {
                'invisible': Not(Equal(Eval('state'), 'done')),
            },
        })

    ## Method to check for availability and make the hospital bed reservation
    # Checks that there are not overlapping dates and status of the bed / room
    # is not confirmed, hospitalized or done but requiring cleaning ('done')
    @classmethod
    @ModelView.button
    def confirmed(cls, records):
        for rec in records:
            rec.set_admission_type()
            rec.get_antecedents()
        reg = records[0]
        Bed = Pool().get('health.hospital.bed')
        cursor = Transaction().connection.cursor()
        bed_id = reg.bed.id
        cursor.execute("SELECT COUNT(*) \
            FROM health_inpatient \
            WHERE (hospitalization_date::timestamp,discharge_date::timestamp) \
                OVERLAPS (timestamp %s, timestamp %s) \
              AND (state = %s or state = %s or state = %s) \
              AND bed = CAST(%s AS INTEGER) ",
            (
                reg.hospitalization_date, reg.discharge_date, 'confirmed',
                'hospitalized', 'done', str(bed_id))
            )
        res = cursor.fetchone()
        if reg.discharge_date.date() < reg.hospitalization_date.date():
            raise UserError(gettext('health_inpatient.msg_discharge_date'))
        if res[0] > 0:
            raise UserError(gettext(
                'health_inpatient.msg_bed_is_not_available'
            ))
        else:
            cls.write(records, {'state': 'confirmed'})
            Bed.write([reg.bed], {'state': 'reserved'})

    @classmethod
    @ModelView.button
    def discharge(cls, records):
        pool = Pool()
        Bed = pool.get('health.hospital.bed')
        sign_prof = pool.get('health.professional').get_health_professional()

        for rec in records:
            rec.update_service()
            rec.set_antecedents()
            rec.set_discharge_hospitalization_date()
            cls.write([rec], {'state': 'done', 'discharged_by': sign_prof})
            Bed.write([rec.bed], {'state': 'to_clean'})

    @classmethod
    @ModelView.button
    def bedclean(cls, records):
        registration_id = records[0]
        Bed = Pool().get('health.hospital.bed')
        cls.write(records, {'state': 'finished'})
        Bed.write([registration_id.bed], {'state': 'free'})

    @classmethod
    @ModelView.button
    def cancel(cls, records):
        registration_id = records[0]
        Bed = Pool().get('health.hospital.bed')
        cls.write(records, {'state': 'cancelled'})
        Bed.write([registration_id.bed], {'state': 'free'})

    @classmethod
    @ModelView.button
    def admission(cls, records):
        for rec in records:
            rec._update_states()
            rec._create_service()
            rec.get_antecedents()

    def get_ward(self, name=None):
        if self.bed and self.bed.ward:
            return self.bed.ward.id

    @classmethod
    def searcher_ward(cls, name=None, clause=None):
        if name == 'ward':
            return [
                ('bed.ward', clause[1], clause[2])
            ]

    def get_service_data(self):
        data = {
            'patient': self.patient.id,
            'institution': self.institution.id,
            'service_date': date.today(),
            'state': 'draft',
            'desc': 'Hospitalization',
            'origin': str(self),
        }
        return data

    def _create_service(self):
        Service = Pool().get('health.service')
        data = self.get_service_data()
        service, = Service.create([data])
        self.health_service = service.id
        self.save()

    def _update_states(self):
        pool = Pool()
        Bed = pool.get('health.hospital.bed')
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company.timezone:
            timez = pytz.timezone(company.timezone)
            dt = datetime.today()
            dt_local = datetime.astimezone(dt.replace(tzinfo=pytz.utc), timez)

            if self.hospitalization_date.date() != dt_local.date():
                raise UserError(gettext(
                    "health_inpatient.msg_date_must_today"))
            else:
                self.state = 'hospitalized'
                self.save()
                Bed.write([self.bed], {'state': 'occupied'})
        else:
            raise UserError(gettext('health_inpatient.msg_need_timezone'))

    def _get_line(self, product, qty, ward=None):
        return {
            'desc': product.template.name,
            'to_invoice': True,
            'product': product.id,
            'unit_price': product.list_price,
            'qty': qty,
            'ward': ward.id if ward else None,
        }

    def update_service(self):
        # After of discharge the service must be updated adding all the
        # resources that patient consumed.
        Service = Pool().get('health.service')
        lines = []
        for med in self.medications:
            lines.append(self._get_line(med.medicament.product, med.qty))
        for lab in self.lab_requests:
            lines.append(self._get_line(lab.test.product, 1))
        for eva in self.evaluations:
            lines.append(self._get_line(eva.product, 1))
        for img in self.imaging_requests:
            lines.append(self._get_line(img.test.product, 1))
        for ms in self.medical_supply:
            lines.append(self._get_line(ms.product, 1))
        for proc in self.procedures:
            lines.append(self._get_line(proc.procedure.product, 1, proc.ward))
        for interc in self.interconsultations:
            lines.append(self._get_line(interc.product, 1))
        if lines:
            Service.write([self.health_service], {
                'lines': [('create', lines)]
            })

    def set_admission_type(self):
        self.admission_type = self.admission_kind
        self.save()

    def set_discharge_hospitalization_date(self):
        self.discharge_hospitalization_date = datetime.now()
        self.save()

    def set_professional_discharged(self):
        # Set te professional that discharged the patient.
        Prof = Pool().get('health.professional')
        self.discharge_attending_physician = Prof.get_health_professional()
        self.save()

    def set_antecedents(self):
        Patient = Pool().get('health.patient')
        Patient.write([self.patient], {
            'disease_history': self.disease_history,
            'childhood_history': self.childhood_history,
            'adolescence_history': self.adolescence_history,
            'adulthood_history': self.adulthood_history,
            'family_history': self.family_history,
            'common_medication': self.common_medication,
            'blood_transfusion': self.blood_transfusion,
            'coffee': self.coffee,
            'coffee_frequency': self.coffee_frequency,
            'smoke': self.smoke,
            'smoke_frequency': self.smoke_frequency,
            'alcohol': self.alcohol,
            'alcohol_frequency': self.alcohol_frequency,
            'other_habits': self.other_habits,
            'medications_allergy': self.medications_allergy,
            'food_allergy': self.food_allergy,
            'other_allergy': self.other_allergy,
            'surgical_history': self.surgical_history,
            'trauma_history': self.trauma_history,
        })
        self.save()

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = config.inpatient_registration_sequence.get()
        return super(Inpatient, cls).create(vlist)

    @staticmethod
    def default_state():
        return 'free'

    @classmethod
    def validate(cls, records):
        super(Inpatient, cls).validate(records)
        for rec in records:
            rec.check_discharge_context()

    def check_discharge_context(self):
        if ((not self.discharge_reason or not self.discharge_dx
            or not self.admission_reason) and self.state == 'done'):
            raise UserError(gettext(
                'health_inpatient.msg_discharge_reason_needed'
            ))

    # Format Registration ID : Patient : Bed
    def get_rec_name(self, name):
        if self.patient and self.bed:
            return self.code + ':' + self.bed.rec_name + ':' \
                + self.patient.rec_name
        else:
            return self.code

    # Allow searching by the hospitalization code, patient name
    # or bed number

    @classmethod
    def search_rec_name(cls, name, clause):
        field = None
        # Search by Registration Code ID or Patient
        for field in ('code', 'patient', 'bed'):
            registrations = cls.search([(field,) + tuple(clause[1:])], limit=1)
            if registrations:
                break
        if registrations:
            return [(field,) + tuple(clause[1:])]
        return [(cls._rec_name,) + tuple(clause[1:])]

    def get_user(self, name=None):
        return Transaction().context.get('user')

    def get_shipments(self, name=None):
        Move = Pool().get('stock.move')
        moves = Move.search([
            ('origin', '=', str(self)),
        ])
        return [m.shipment.id for m in moves if m.shipment]

    def get_antecedents(self):
        self.disease_history = self.patient.disease_history
        self.childhood_history = self.patient.childhood_history
        self.adolescence_history = self.patient.adolescence_history
        self.adulthood_history = self.patient.adulthood_history
        self.family_history = self.patient.family_history
        self.common_medication = self.patient.common_medication
        self.blood_transfusion = self.patient.blood_transfusion
        self.coffee = self.patient.coffee
        self.coffee_frequency = self.patient.coffee_frequency
        self.smoke = self.patient.smoke
        self.smoke_frequency = self.patient.smoke_frequency
        self.alcohol = self.patient.alcohol
        self.alcohol_frequency = self.patient.alcohol_frequency
        self.other_habits = self.patient.other_habits
        self.medications_allergy = self.patient.medications_allergy
        self.food_allergy = self.patient.food_allergy
        self.other_allergy = self.patient.other_allergy
        self.surgical_history = self.patient.surgical_history
        self.trauma_history = self.patient.trauma_history
        self.save()

    def generate_shipment(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        Date = pool.get('ir.date')
        to_create = []
        moves = self.get_stock_moves(self.medications)
        if not moves:
            return
        to_create.append({
            'planned_date': Date.today(),
            'company': Transaction().context.get('company'),
            'customer': self.patient.party.id,
            'reference': self.code,
            'warehouse': self.hospitalization_location.id,
            'delivery_address': self.patient.party.addresses[0].id,
            'moves': [('add', moves)],
        })
        if to_create:
            Shipment.create(to_create)

    def get_stock_moves(self, medications):
        pool = Pool()
        Move = pool.get('stock.move')
        moves = []
        for medication in medications:
            move_info = {}
            move_info['origin'] = str(self)
            move_info['product'] = medication.medicament.product.id
            move_info['uom'] = medication.medicament.product.default_uom.id
            move_info['quantity'] = medication.qty
            move_info['from_location'] = self.hospitalization_location.output_location.id
            move_info['to_location'] = self.patient.party.customer_location.id
            move_info['unit_price'] = medication.medicament.product.list_price
            move_info['state'] = 'draft'
            moves.append(move_info)
        new_moves = Move.create(moves)
        return new_moves

    @fields.depends('patient')
    def on_change_patient(self, name=None):
        if self.patient and self.patient.party.puid:
            self.puid = self.patient.party.puid


class Evolutions(ModelSQL, ModelView):
    'Evolutions'
    __name__ = 'health.inpatient.evolutions'

    registration = fields.Many2One('health.inpatient', 'Registration Code')
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True)
    attending_date = fields.DateTime('Attending Date', readonly=True)
    evolution_note = fields.Text('Evolution')
    evolution_state = fields.Selection([
        (None, ''),
        ('improvement', 'Evidence Improvement'),
        ('not_improvement', 'Has Not Improved'),
        ('worsened', 'Has gotten worse'),
        ], 'Evolution State', select=True)
    evolution_state_string = evolution_state.translated('evolution_state ')

    @classmethod
    def __setup__(cls):
        super(Evolutions, cls).__setup__()
        cls._buttons.update({
                'print_evolution': {},
                })

    @classmethod
    @ModelView.button_action('health_inpatient.report_inpatient_evolution')
    def print_evaluation(cls, records):
        pass

    @staticmethod
    def default_attending_date():
        return datetime.now()

    @classmethod
    def default_attending_physician(cls):
        Professional = Pool().get('health.professional')
        cls.attending_physician = Professional.get_health_professional()
        return cls.attending_physician


class NewBorn(ModelSQL, ModelView):
    'NewBorn'
    __name__ = 'health.inpatient.newborn'

    registration = fields.Many2One('health.inpatient', 'Registration Code')
    newborn_name = fields.Char('NewBorn Name')
    gestational_age = fields.Integer('Gestational Age')
    sex = fields.Selection([
        (None, ''),
        ('male', 'Male'),
        ('female', 'Female'),
        ], 'Sex', select=True)
    weight_kgs = fields.Integer('Weight/kgs')
    weight_lbs = fields.Integer('Weight/lbs')
    weight_grams = fields.Integer('Weight/grams')
    height = fields.Integer('Height')
    head_circunference = fields.Integer('Head Circunference')
    thoracic_circunference = fields.Integer('Thoracic Circunference')
    apgar_diagnosis = fields.Integer('APGAR')
    birth_type = fields.Char('Birth Type')
    hearth = fields.Char('Hearth')
    lungs = fields.Char('Lungs')
    genitals = fields.Char('Genitals')
    abdomen = fields.Char('Abdomen')
    anus = fields.Char('Anus')
    plantar_folds = fields.Char('Plantar Folds')
    fleshing   = fields.Boolean('Fleshing')
    fleshing_description = fields.Text('Fleshing Description', states={
        'required': Bool(Eval('fleshing')),
        'invisible': ~Bool(Eval('fleshing'))
    })
    congenital_anomalies = fields.Boolean('Congenital Anomalies')
    congenital_description = fields.Text('Congenital Description', states={
        'required': Bool(Eval('congenital_anomalies')),
        'invisible': ~Bool(Eval('congenital_anomalies'))
    })
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True)
    medic_team = fields.One2Many('health.inpatient.medic_team',
        'registration', 'Medic Team')
    newborn_diagnosis = fields.Many2One('health.pathology', 'Diagnosis')
    attending_date = fields.DateTime('Attending Date', readonly=True)
    observation_note = fields.Text('Observation NewBorn Receivement')

    @classmethod
    def __setup__(cls):
        super(NewBorn, cls).__setup__()
        cls._buttons.update({
                'print_newborn': {},
                })

    @classmethod
    @ModelView.button_action('health_inpatient.report_newborn_data')
    def print_newborn(cls, records):
        pass

    @staticmethod
    def default_attending_date():
        return datetime.now()

    @classmethod
    def default_attending_physician(cls):
        Professional = Pool().get('health.professional')
        cls.attending_physician = Professional.get_health_professional()
        return cls.attending_physician


class BedTransfer(ModelSQL, ModelView):
    'Bed transfers'
    __name__ = 'health.bed.transfer'
    registration = fields.Many2One('health.inpatient',
        'Registration Code')
    transfer_date = fields.DateTime('Date')
    bed_from = fields.Many2One('health.hospital.bed', 'From')
    bed_to = fields.Many2One('health.hospital.bed', 'To')
    reason = fields.Char('Reason')


class PatientEvaluation(ModelSQL, ModelView):
    __name__ = 'health.patient.evaluation'

    @classmethod
    def _get_origin(cls):
        return super(PatientEvaluation, cls)._get_origin() + [
            'health.inpatient'
        ]


class ECG(ModelSQL, ModelView):
    __name__ = 'health.patient.ecg'

    inpatient_registration_code = fields.Many2One(
        'health.inpatient', 'Inpatient Registration',
        help="Enter the patient hospitalization code")


class PatientData(ModelSQL, ModelView):
    'Inherit patient model and add the patient status to the patient.'
    __name__ = 'health.patient'

    patient_status = fields.Function(fields.Boolean('Hospitalized',
        help="Show the hospitalization status of the patient"),
        getter='get_patient_status', searcher='search_patient_status')

    @classmethod
    def get_patient_status(cls, patients, name):
        cursor = Transaction().connection.cursor()
        Registration = Pool().get('health.inpatient')
        registration = Registration.__table__()

        # Will store statuses {patient: True/False, ...}
        ids = list(map(int, patients))
        result = dict.fromkeys(ids, False)

        for sub_ids in grouped_slice(ids):
            # SQL expression for relevant patient ids
            clause_ids = reduce_ids(registration.id, sub_ids)

            # Hospitalized patient ids
            query = registration.select(registration.patient, Literal(True),
                where=(registration.state == 'hospitalized') & clause_ids,
                group_by=registration.patient)

            # Update dictionary of patient ids with True statuses
            cursor.execute(*query)
            result.update(cursor.fetchall())

        return result

    @classmethod
    def search_patient_status(cls, name, clause):
        p = Pool().get('health.inpatient')
        table = p.__table__()
        pat = cls.__table__()
        _, operator, value = clause

        # Validate operator and value
        if operator not in ['=', '!=']:
            raise ValueError('Wrong operator: %s' % operator)
        if value is not True and value is not False:
            raise ValueError('Wrong value: %s' % value)

        # Find hospitalized patient ids
        j = pat.join(table, condition=pat.id == table.patient)
        s = j.select(pat.id, where=table.state == 'hospitalized')

        # Choose domain operator
        if (operator == '=' and value) or (operator == '!=' and not value):
            d = 'in'
        else:
            d = 'not in'

        return [('id', d, s)]


class InpatientMedication (ModelSQL, ModelView):
    'Inpatient Medication'
    __name__ = 'health.inpatient.medication'
    registration = fields.Many2One('health.inpatient', 'Inpatient Code',
        required=True)
    medicament = fields.Many2One('health.medicament', 'Medicament',
        required=True, help='Prescribed Medicament')
    indication = fields.Many2One('health.pathology', 'Indication',
        help='Choose a disease for this medicament from the disease list. It'
        ' can be an existing disease of the patient or a prophylactic.')
    start_treatment = fields.DateTime('Start',
        help='Date of start of Treatment', required=True)
    end_treatment = fields.DateTime('End', help='Date of start of Treatment')
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose', required=True)
    dose_unit = fields.Many2One('health.dose.unit', 'dose unit',
        required=True, help='Unit of measure for the medication to be taken')
    route = fields.Many2One('health.drug.route', 'Administration Route',
        required=True, help='Drug administration route code.')
    form = fields.Many2One('health.drug.form', 'Form', required=True,
        help='Drug form, such as tablet or gel')
    qty = fields.Integer('x', required=True,
        help='Quantity of units (eg, 2 capsules) of the medicament')
    common_dosage = fields.Many2One('health.medication.dosage', 'Frequency',
        help='Common / standard dosage frequency for this medicament')
    log_history = fields.One2Many('health.inpatient.medication.log',
        'medication', "Log History")
    frequency = fields.Integer('Frequency',
        help='Time in between doses the patient must wait (ie, for 1 pill'
        ' each 8 hours, put here 8 and select \"hours\" in the unit field')
    frequency_unit = fields.Selection([
        (None, ''),
        ('seconds', 'seconds'),
        ('minutes', 'minutes'),
        ('hours', 'hours'),
        ('days', 'days'),
        ('weeks', 'weeks'),
        ('wr', 'when required'),
        ], 'unit', select=True, sort=False)
    frequency_prn = fields.Boolean('PRN',
        help='Use it as needed, pro re nata')
    is_active = fields.Boolean('Active',
        help='Check if the patient is currently taking the medication')
    discontinued = fields.Boolean('Discontinued')
    course_completed = fields.Boolean('Course Completed')
    discontinued_reason = fields.Char('Reason for discontinuation',
        states={
            'invisible': Not(Bool(Eval('discontinued'))),
            'required': Bool(Eval('discontinued')),
        },
        depends=['discontinued'],
        help='Short description for discontinuing the treatment')
    adverse_reaction = fields.Text('Adverse Reactions',
        help='Side effects or adverse reactions that the patient experienced')

    @fields.depends('discontinued', 'course_completed')
    def on_change_with_is_active(self):
        is_active = True
        if (self.discontinued or self.course_completed):
            is_active = False
        return is_active

    @fields.depends('is_active', 'course_completed')
    def on_change_with_discontinued(self):
        return not (self.is_active or self.course_completed)

    @fields.depends('is_active', 'discontinued')
    def on_change_with_course_completed(self):
        return not (self.is_active or self.discontinued)

    @staticmethod
    def default_is_active():
        return True

    def get_rec_name(self, name):
        if self.medicament:
            return self.medicament.description


class InpatientMedicationLog (ModelSQL, ModelView):
    'Inpatient Medication Log History'
    __name__ = "health.inpatient.medication.log"
    medication = fields.Many2One('health.inpatient.medication', 'Medication',
        required=True)
    admin_time = fields.DateTime("Admin Time")
    health_professional = fields.Many2One('health.professional',
        'Health Professional', readonly=True)
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose')
    dose_unit = fields.Many2One('health.dose.unit', 'Dose Unit',
        help='Unit of measure for the medication to be taken')
    remarks = fields.Text('Remarks', help='specific remarks for this dose')
    shift = fields.Many2One('health.inpatient.shift', 'Shift')

    @classmethod
    def __setup__(cls):
        super(InpatientMedicationLog, cls).__setup__()

    @classmethod
    def validate(cls, records):
        super(InpatientMedicationLog, cls).validate(records)
        for record in records:
            record.check_health_professional()

    def check_health_professional(self):
        if not self.health_professional:
            raise UserWarning(gettext('health.msg_health_professional_warning'))

    @staticmethod
    def default_health_professional():
        return Pool().get('health.professional').get_health_professional()

    @staticmethod
    def default_admin_time():
        return datetime.now()


class InpatientDiet(ModelSQL, ModelView):
    'Inpatient Diet'
    __name__ = "health.inpatient.diet"
    registration = fields.Many2One('health.inpatient', 'Registration Code')
    diet = fields.Many2One('health.diet.therapeutic', 'Diet', required=True)
    remarks = fields.Text('Remarks / Directions',
        help='specific remarks for this diet / patient')


class InpatientMeal(ModelSQL, ModelView):
    'Inpatient Meal'
    __name__ = "health.inpatient.meal"
    product = fields.Many2One('product.product', 'Food', required=True,
        help='Food')
    diet_therapeutic = fields.Many2One('health.diet.therapeutic', 'Diet')
    diet_belief = fields.Many2One('health.diet.belief', 'Belief')
    diet_vegetarian = fields.Many2One('health.vegetarian_types', 'Vegetarian')
    institution = fields.Many2One('health.institution', 'Institution')

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    def get_rec_name(self, name):
        if self.product:
            return self.product.name


class InternalProcedure(ModelSQL, ModelView):
    'Internal Procedure'
    __name__ = 'health.internal_procedure'
    origin = fields.Reference('Origin', selection='get_origin', required=True,
        states={'readonly': True})
    procedure = fields.Many2One('health.procedure', 'Procedure', required=True)
    ward = fields.Many2One('health.hospital.ward', 'Ward')
    comments = fields.Char('Comments')

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.inpatient']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]


class MedicalSupply(ModelSQL, ModelView):
    'Medical Supply'
    __name__ = "health.medical_supply"
    STATES = {'readonly': Eval('state') != 'draft'}

    product = fields.Many2One('product.product', 'Product', required=True,
        states=STATES, domain=[
            ('kind', '=', 'medical_supply'),
        ])
    quantity = fields.Float('Quantity', required=True, states=STATES)
    request_date = fields.Date('Date', required=True, states=STATES)
    origin = fields.Reference('Origin', selection='get_origin', required=True,
        states={'readonly': True})
    notes = fields.Char('Notes', states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ], 'State', readonly=True)

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.inpatient']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @staticmethod
    def default_request_date():
        return date.today()

    @staticmethod
    def default_state():
        return 'draft'


class InpatientMealOrderItem (ModelSQL, ModelView):
    'Inpatient Meal Item'
    __name__ = "health.inpatient.meal.order.item"
    meal_order = fields.Many2One('health.inpatient.meal.order', 'Meal Order')
    meal = fields.Many2One('health.inpatient.meal', 'Meal')
    remarks = fields.Char('Remarks')


class InpatientMealOrder (ModelSQL, ModelView):
    'Inpatient Meal Order'
    __name__ = "health.inpatient.meal.order"
    registration = fields.Many2One('health.inpatient',
        'Registration Code', domain=[('state', '=', 'hospitalized')],
        required=True)
    mealtime = fields.Selection((
        (None, ''),
        ('breakfast', 'Breakfast'),
        ('lunch', 'Lunch'),
        ('dinner', 'Dinner'),
        ('snack', 'Snack'),
        ('special', 'Special order'),
        ), 'Meal time', required=True, sort=False)
    meal_item = fields.One2Many('health.inpatient.meal.order.item', 'name',
        'Items')
    meal_order = fields.Char('Order', readonly=True)
    health_professional = fields.Many2One('health.professional',
        'Health Professional')
    remarks = fields.Text('Remarks')
    meal_warning = fields.Boolean('Warning',
        help="The patient has special needs on meals")
    meal_warning_ack = fields.Boolean('Ack',
        help="Check if you have verified the warnings on the"
        " patient meal items")
    order_date = fields.DateTime('Date', help='Order date', required=True)
    state = fields.Selection((
        (None, ''),
        ('draft', 'Draft'),
        ('cancelled', 'Cancelled'),
        ('ordered', 'Ordered'),
        ('processing', 'Processing'),
        ('done', 'Done'),
        ), 'Status', readonly=True)

    @staticmethod
    def default_order_date():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_health_professional():
        return Pool().get('health.professional').get_health_professional()

    @classmethod
    def create(cls, vlist):
        Config = Pool().get('health.configuration')

        vlist = [x.copy() for x in vlist]
        config = Config().get_config()
        for values in vlist:
            if values.get('meal_order'):
                continue
            values['meal_order'] = config.inpatient_meal_order_sequence.get()
        return super(InpatientMealOrder, cls).create(vlist)

    @classmethod
    def validate(cls, meal_orders):
        super(InpatientMealOrder, cls).validate(meal_orders)
        for meal_order in meal_orders:
            meal_order.check_meal_order_warning()
            meal_order.check_health_professional()

    def check_meal_order_warning(self):
        if not self.meal_warning_ack and self.meal_warning:
            raise UserError(gettext('health_inpatient.msg_meal_order_warning'))

    def check_health_professional(self):
        if not self.health_professional:
            raise UserError(gettext('health.msg_no_professional_associated_user'))

    @fields.depends('registration')
    def on_change_registration(self):
        if self.registration:
            # Trigger the warning if the patient
            # has special needs on meals (religion / philosophy )
            if (self.registration.patient.vegetarian_type or
                self.registration.patient.diet_belief):
                self.meal_warning = True
                self.meal_warning_ack = False

    @classmethod
    def __setup__(cls):
        super(InpatientMealOrder, cls).__setup__()
        cls._buttons.update({
            'cancel': {'invisible': Not(Equal(Eval('state'), 'ordered'))}
        })
        cls._buttons.update({
            'generate': {'invisible': Or(Equal(Eval('state'), 'ordered'),
                Equal(Eval('state'), 'done'))},
        })
        cls._buttons.update({
            'done': {'invisible': Not(Equal(Eval('state'), 'ordered'))}
        })

        t = cls.__table__()
        cls._sql_constraints = [
            ('meal_order_uniq', Unique(t, t.meal_order),
                'The Meal Order code already exists'),
        ]

    @classmethod
    @ModelView.button
    def generate(cls, mealorders):
        cls.write(mealorders, {'state': 'ordered'})

    @classmethod
    @ModelView.button
    def cancel(cls, mealorders):
        cls.write(mealorders, {'state': 'cancelled'})

    @classmethod
    @ModelView.button
    def done(cls, mealorders):
        cls.write(mealorders, {'state': 'done'})


class Interconsultation(metaclass=PoolMeta):
    __name__ = 'health.interconsultation'

    @classmethod
    def _get_origin(cls):
        return super(Interconsultation, cls)._get_origin() + ['health.inpatient']


class HealthLabTest(metaclass=PoolMeta):
    __name__ = 'health.lab_test'

    @classmethod
    def _get_origin(cls):
        return super(HealthLabTest, cls)._get_origin() + ['health.inpatient']


class ImagingTestRequest(metaclass=PoolMeta):
    __name__ = 'health.imaging.request'

    @classmethod
    def _get_origin(cls):
        return super(ImagingTestRequest, cls)._get_origin() + ['health.inpatient']


class HealthService(metaclass=PoolMeta):
    __name__ = 'health.service'

    @classmethod
    def _get_origin(cls):
        return super(HealthService, cls)._get_origin() + [
            'health.inpatient'
        ]

class MedicTeam(ModelSQL, ModelView):
    'Medic Team'
    __name__ = 'health.inpatient.medic_team'
    _rec_name = 'attending_physician'

    registration = fields.Many2One('health.inpatient', 'Inpatient Code')
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician')

class EvolutionsReport(Report):
    'Evolutions Report'
    __name__ = 'health_inpatient.inpatient.evolutions'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(EvolutionsReport, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context
