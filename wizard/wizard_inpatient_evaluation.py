from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.i18n import gettext
from trytond.exceptions import UserError


class CreateInpatientEvaluation(Wizard):
    'Create Inpatient Evaluation'
    __name__ = 'wizard.health.inpatient.evaluation'

    start_state = 'inpatient_evaluation'
    inpatient_evaluation = StateAction('health_inpatient.act_inpatient_evaluation')

    def do_inpatient_evaluation(self, action):
        inpatient_registration = Transaction().context.get('active_id')
        Registration = Pool().get('health.inpatient')
        try:
            reg = Registration.browse([inpatient_registration])[0]
        except:
            raise UserError(gettext('health_inpatient.msg_no_record_selected'))

        patient = reg.patient.id

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', patient),
            ('origin', '=', str(reg)),
            ('evaluation_type', '=', 'inpatient'),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient,
            'origin': str(reg),
            'evaluation_type': 'inpatient',
        })

        return action, {}

    @classmethod
    def __setup__(cls):
        super(CreateInpatientEvaluation, cls).__setup__()
